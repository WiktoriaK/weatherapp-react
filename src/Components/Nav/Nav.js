import React from 'react';
import './Nav.css';

const Nav = ({ updateInputValue, searchInputValue, onCity, onLocation }) => {
    return (
        <nav className='app-nav'>
            <h1>Aplikacja Pogodowa</h1>
            <input className='app-nav-input' onChange={updateInputValue} 
                value={searchInputValue.charAt(0).toUpperCase() + searchInputValue.slice(1)
            }/>
            <button className='app-nav-btn' onClick={onCity}>Szukaj</button>
            <button className='app-nav-button' onClick={onLocation} >Twoja Pogoda</button>
        </nav>
    )
}


export default Nav;