import React from 'react';


const ResultWeathers = ({ weatherImage , weatherImageAlt, weather}) => {
    const { id } = weather;
    return (
        <li key={weather.id}>{Math.round(weather.max_temp)}°
            <img className='weather-img-days' src={weatherImage} alt={weatherImageAlt}/>
        </li>
    )
}


export default ResultWeathers;