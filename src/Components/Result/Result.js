import React from 'react';
import './Result.css';

const Result = ({ city, date, weatherImage, weatherImageAlt, temp, maxTem , minTem, humidity, visibility, wind_speed }) => {
    return (
      <div>
            <h1> {city} </h1>
            <img className='result-weather-img' src={weatherImage} alt={weatherImageAlt} />
            <div className='result-temperature'>
              <p className='temperature-one'>Temperatura: { temp}°</p>
              <p>Max: {maxTem}°</p>
              <p>Min: {minTem}°</p>
            </div>
            <div className='result-others'>
              <p>Wilgotność: {humidity}% </p>
              <p>Widoczność: {visibility}% </p>
              <p>Wiatr: {wind_speed} MPH </p>
              <p className='result-data'>Data: {date} </p>
            </div>
      </div> 
    )
}


export default Result;