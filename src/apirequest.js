import axios from 'axios';


export const getWeatherByCityName = cityName => {
    return axios.get(`https://www.metaweather.com/api/location/search/?query=${cityName}`).then(resp => {
        const woeid = getWoeid(resp);
        return axios.get(`https://www.metaweather.com/api/location/${woeid}/`).then(resp => resp.data)
    })
}


export const getWeatherLocation = (lat, lon) => {
    return axios.get(`https://www.metaweather.com/api/location/search/?lattlong=${lat},${lon}`).then(resp => {
        const { woeid } = resp.data[0];
        return axios.get(`https://www.metaweather.com/api/location/${woeid}/`).then(resp => resp.data)
    });
}

const getWoeid = (resp) => {

    try {
      const { woeid } = resp.data[0];
      return woeid;
  
    } catch {
      alert('Wpisana nazwa jest niepoprawna.')
    }
  }
  






