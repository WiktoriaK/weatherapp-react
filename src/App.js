import React from 'react';
import './App.css';
import {getWeatherByCityName, getWeatherLocation} from './apirequest';
import Nav from './Components/Nav/Nav';
import Result from './Components/Result/Result';
import Loader from './Components/Loader/Loader';
import Footer from './Components/Footer/Footer';




class App extends React.Component {
  constructor() {
    super();


    this.state = {
      searchInputValue: '',
      weatherData: {
        cityName: '',
        currTemp: '',
        maxTemp: '',
        minTemp: '',
        date: '',
        humidity: '',
        visibility: '',
        weatherImage: '',
        weatherImageAlt: '',
        consolidatedWeather: [],
        isLoading: true,
        wind: ''
      }
    
    }
  }

  updateInputValue = (event) => {
    this.setState({searchInputValue: event.target.value})
    
  }

  getWeatherByCity = () => {
    const { searchInputValue: cityName } = this.state;
    this.setState({ isLoading: true });
    getWeatherByCityName(cityName).then(resp => {
      this.setState({searchInputValue: ''});
      this.displayWeatherData(resp);
    });
  }

  getWeatherLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(location => {
        const { latitude, longitude } = location.coords;
        this.setState({ isLoading: true });
        getWeatherLocation(latitude, longitude).then(resp => {
          this.displayWeatherData(resp);
          
        })
      })
    } else {
      alert('Ta funkcja nie zadziała w Twojej przeglądarce.');
    }
  }


  displayWeatherData = weatherData => {
    const [weather, ...weathers] = weatherData.consolidated_weather;
    const imgCode = weather.weather_state_abbr;
    const { weather_state_name: imgAlt } = weather;
    const imgSrc = `https://www.metaweather.com/static/img/weather/${imgCode}.svg`;
  
    this.setState({
      isLoading: false,
      weatherData: {
        cityName: weatherData.title,
        currTemp: Math.round(weather.the_temp),
        maxTemp: Math.round(weather.max_temp),
        minTemp: Math.round(weather.min_temp),
        date: weather.applicable_date,
        humidity: weather.humidity,
        visibility: Math.round(weather.visibility),
        wind: Math.round(weather.wind_speed),
        weatherImage: imgSrc,
        weatherImageAlt: imgAlt,
        consolidatedWeather: weathers,
      }
      
    })
    
    
  }  

  render() {

    return (
      <> 
      <div className='app'>
      
        <Nav
          updateInputValue={this.updateInputValue}
          searchInputValue={this.state.searchInputValue}
          onCity={this.getWeatherByCity}
          onLocation={this.getWeatherLocation}
        />
      {this.state.isLoading && (<div className="loader"> <Loader /> </div> )}
        <main>
          
          { this.state.weatherData.cityName && ( 
            <Result
              city={this.state.weatherData.cityName}
              date={this.state.weatherData.date} 
              weatherImage={this.state.weatherData.weatherImage}
              weatherImageAlt={this.state.weatherData.weatherImageAlt}
              temp={this.state.weatherData.currTemp}
              maxTem={this.state.weatherData.maxTemp}
              minTem={this.state.weatherData.minTemp}
              humidity={this.state.weatherData.humidity}
              visibility={this.state.weatherData.visibility} 
              wind_speed={this.state.weatherData.wind}     
            />
          
          )}

          
           <ul>
           { this.state.weatherData.consolidatedWeather.map(weather => {
               return <li key={weather.id}> {Math.round(weather.max_temp)}°
                 <img className='weather-img-days' 
                  src={`https://www.metaweather.com/static/img/weather/${weather.weather_state_abbr}.svg`} 
                  alt={weather.weather_state_name}
                 />
               </li>
             }) }
           </ul>
           
        </main>

        <Footer />

      </div>
        

      </>

    );
  }
}


export default App;
